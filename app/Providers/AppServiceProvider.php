<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Models\Service;
use App\Models\Brand;
use App\Models\Setting;
use App\Models\SiteContent;
use App\Models\Blog;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // services
        $services = Service::select('name','category_id','img','slug','price','small_description')
        ->where('language_id',lang_front())
        ->where('status','yes')
        ->where('show_in_homePage','yes')->take(8)->get();

        // settings
        $setting = Setting::where('language_id',lang_front())->first();
        $site = SiteContent::where('language_id',lang_front())->first();
        $site_content =  json_decode($site->site_content);

        // brands
        $brands = Brand::select('img','name')
        ->where('language_id',lang_front())->where('status','yes')
        ->take(8)->get();

        $footerPosts = Blog::select('name','slug','created_at')
        ->where('language_id',lang_front())->latest()->take(2)->get();



        \View::share(['services'=>$services,'setting'=>$setting,
            'brands'=>$brands,
            'site_content'=>$site_content,
            'footerPosts'=>$footerPosts]);
    
    
    
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
