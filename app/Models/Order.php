<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name','phone','email','message','service_id','type'];


    public function service()
    {
    	return $this->hasOne('App\Models\Service','id','service_id');
    }



    public function product()
    {
    	return $this->hasOne('App\Models\Product','id','service_id');
    }


    
}
