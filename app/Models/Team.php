<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name','language_id','img','desc','position',
    'facebook','instagram','twitter','linkedin'];
    
}
