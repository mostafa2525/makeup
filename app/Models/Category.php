<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    protected $fillable = ['name','language_id','img','description','slug','show_in_homePage','small_description'];

    public function services()
    {
    	return $this->hasMany('App\Models\Service','category_id','id');
    }

}
