<?php 

namespace App\Helpers\Classes;

use Image;
class UploadClass
{
	
	public static function uploadFile($request,$name_file,$path,$all=null,$big=700)
	{
        // upload one file and return name of this file after change the name of this file  
            // create instanc
        
        // add more size of image ( small , medium , big )
        if($all)
        {
            // small -> in small folder
            Image::make($request->$name_file)->resize(300, null, function ($constraint) 
            {
                $constraint->aspectRatio();
            })->save($path.'small/'.$request->$name_file->hashName());


            // medium -> in medium folder
            Image::make($request->$name_file)->resize(500, null, function ($constraint) 
            {
                $constraint->aspectRatio();
            })->save($path.'medium/'.$request->$name_file->hashName());

            //  big  -> in the root of path 
            Image::make($request->$name_file)->resize($big, null, function ($constraint) 
            {
                $constraint->aspectRatio();
            })->save($path.$request->$name_file->hashName());


        }
        else
        {
            Image::make($request->$name_file)->resize('300', null, function ($constraint) 
            {
                $constraint->aspectRatio();
            })->save($path.$request->$name_file->hashName());

        }


        return $request->$name_file->hashName();
     
	}


    //	upload image withoutResize and with resizing
    // $hS -> height for small
    // $hM -> height for medium
	public  static  function uploadImage($request,$name_file,$path,$small=null,$medium=null,$hS=null,$hM=null)
    {

        if($small){$s=$small;}else{$s='400';}
        if($medium){$m=$medium;}else{$m='600';}
        // small -> in small folder
        Image::make($request->$name_file)->resize($s, $hS, function ($constraint) 
        {
            // $constraint->aspectRatio();
        })->save($path.'small/'.$request->$name_file->hashName());

        // medium -> in medium folder
        Image::make($request->$name_file)->resize($m, $hM, function ($constraint) 
        {
            // $constraint->aspectRatio();
        })->save($path.'medium/'.$request->$name_file->hashName());

        // upload image using intervention without resizeing
        $img = Image::make($request->$name_file);
        // save image
        $img->save($path.$request->$name_file->hashName());

        return $request->$name_file->hashName();
    }








    // upload one image withoutResize
	public  static  function uploadRealImage($request,$name_file,$path)
    {
        // upload image using intervention without resizeing
        $img = Image::make($request->$name_file);
        // save image
        $img->save($path.$request->$name_file->hashName());
        return $request->$name_file->hashName();
    }




    // upload one image with  ( width and height)
    public  static  function uploadOneImage($request,$name_file,$path,$w,$h=null)
    {
        Image::make($request->$name_file)->resize($w, $h, function ($constraint) 
        {
            // $constraint->aspectRatio();
        })->save($path.$request->$name_file->hashName());
        return $request->$name_file->hashName();
    }
















}





























