<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [
                        'name'              => 'required|string|max:190',
                        'desc'              => 'required|string|max:3000',
                        'img'               => 'required|image|mimes:png,jpg,jpeg,gif|max:10000',
                        'position'          => 'required|string|max:100',


                    ];


                break;


            //  editing data 
            case 'PUT':
                        
                        return [
                        'name'              => 'required|string|max:190',
                        'desc'              => 'required|string|max:3000',
                        'img'               => 'nullable|image|mimes:png,jpg,jpeg,gif|max:10000',
                        'position'          => 'required|string|max:100',

                    ];


                break;
        }



    }
}
