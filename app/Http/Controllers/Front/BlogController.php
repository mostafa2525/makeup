<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Blog;


class BlogController extends Controller
{
    
    //
    public function index()
    {
        $data['posts'] = Blog::select('name','slug','small_description','img','created_at')
        ->where('status','yes')
        ->where('language_id',lang_front())->paginate(15);

        $data['latest'] = Blog::select('name','slug')
        ->where('language_id',lang_front())->latest()->take(15)->get();

        return view('front.blog.index')->with($data);
    }



    //
    public function search(Request $request)
    {   

        if($request->search && $request->search != '')
        {
            $data['posts'] = Blog::select('name','slug','small_description','img','created_at')
            ->where('status','yes')
            ->where('tags','Like','%'.$request->search.'%')
            ->orWhere('name','Like','%'.$request->search.'%')
            ->orWhere('small_description','Like','%'.$request->search.'%')
            ->orWhere('description','Like','%'.$request->search.'%')
            ->where('language_id',lang_front())->paginate(15);
        }
        else
        {
            $data['posts'] = Blog::select('name','slug','small_description','img','created_at')
            ->where('status','yes')
            ->where('language_id',lang_front())->paginate(15);
        }

        

        $data['latest'] = Blog::select('name','slug','created_at')
        ->where('language_id',lang_front())->latest()->take(6)->get();

        return view('front.blog.index')->with($data);
    }




    // view data of specific blog 
    public function show($slugBlog)
    {
        $check = Blog::where('language_id',lang_front())
        ->where('status','yes')->where('slug',$slugBlog)->first();
        if($check)
        {
            $data['latest'] = Blog::select('name','slug')->where('status','yes')
            ->where('language_id',lang_front())->latest()->take(6)->get();
            $data['row'] = $check;

            $data['posts'] = Blog::select('name','slug','created_at')
            ->where('status','yes')
            ->where('language_id',lang_front())->take(4)->latest()->get();



            return view('front.blog.show')->with($data);
        }
    }

}
