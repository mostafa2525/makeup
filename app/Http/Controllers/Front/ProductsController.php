<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Product;
use App\Models\Order;


class ProductsController extends Controller
{
    
    // base function for admin 
    public function all()
    {
        $data['allProducts'] = Product::where('language_id',lang_front())->where('status','yes')->paginate(16);
        return view('front.products.index')->with($data);
    }

    // view data of specific service 
    public function show($slugProduct)
    {
        $check = Product::where('language_id',lang_front())
        ->where('status','yes')->where('slug',$slugProduct)->first();
        if($check)
        {
            $data['row'] = $check;
            return view('front.products.show')->with($data);
        }
    }




    // send message   
    public function order(Request $request)
    {
        // dd("zfvdvd");

        if($request->ajax())
        {

            $request->validate([

                'product_id' => 'required|numeric|exists:products,id',
                'name' => 'required|string|max:100',
                'phone' => 'required|numeric|digits_between:6,15',
                'email' => 'required|email|max:100',
                'message' => 'required|string|max:2000',
            ]);


            $data = $request->except("_token");
            
            $data['type'] = "product";
            $data['service_id'] = $request->product_id;
            Order::create($data);
            $message['success'] = trans('frontSite.successOrderMessage');
            return response()->json($message);
        }
    }





}
