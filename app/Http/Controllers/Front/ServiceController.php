<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Service;
use App\Models\Order;


class ServiceController extends Controller
{
    
    // base function for admin 
    public function all()
    {
        $data['allServices'] = Service::where('language_id',lang_front())->where('status','yes')->paginate(16);
        return view('front.services.index')->with($data);
    }






     //
    public function search(Request $request)
    {   

        if($request->search && $request->search != '')
        {
            $data['allServices'] = Service::select('name','slug','small_description','img','created_at','price')
            ->where('status','yes')
            ->where('name','Like','%'.$request->search.'%')
            ->orWhere('small_description','Like','%'.$request->search.'%')
            ->orWhere('description','Like','%'.$request->search.'%')
            ->where('language_id',lang_front())->paginate(16);
            

        }
        else
        {
            $data['allServices'] = Service::where('language_id',lang_front())
            ->select('name','slug','small_description','img','created_at')
            ->where('status','yes')->paginate(16);
        }



        return view('front.services.index')->with($data);
    }







    // view data of specific service 
    public function category($slugcat)
    {
        $check = Category::where('language_id',lang_front())->where('slug',$slugcat)
        ->where('status','yes')->first();
        if($check)
        {
            $data['row'] = $check;
            $data['catService'] = Service::where('category_id',$check->id)
            ->where('status','yes')->paginate(15);
            return view('front.services.category')->with($data);
        }
    }


    // view data of specific service 
    public function show($slugService)
    {
        $check = Service::where('language_id',lang_front())
        ->where('status','yes')->where('slug',$slugService)->first();
        if($check)
        {
            $data['row'] = $check;
            return view('front.services.show')->with($data);
        }
    }




    // send message   
    public function order(Request $request)
    {
        // dd("zfvdvd");

        if($request->ajax())
        {

            $request->validate([

                'service_id' => 'required|numeric|exists:services,id',
                'name' => 'required|string|max:100',
                'phone' => 'required|numeric|digits_between:6,15',
                'email' => 'required|email|max:100',
                'message' => 'required|string|max:2000',
            ]);

            $data = $request->except("_token");
            $data['type'] = 'services';


            Order::create($data);
            $message['success'] = trans('frontSite.successOrderMessage');
            return response()->json($message);
        }
    }





}
