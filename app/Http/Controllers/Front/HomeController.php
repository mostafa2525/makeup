<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Slider;
use App\Models\Product;
use App\Models\Category;
use App\Models\Testimonial;
use App\Models\Team;


class HomeController extends Controller
{
    
    // base function for admin 
    public function index()
    {
        $data['slider'] = Slider::select('name','description','img','link')
        ->where('status','yes')
        ->where('language_id',lang_front())->take(3)->get();

        // Products
        $data['products'] = Product::select('name','category_id','img','slug','price')
        ->where('language_id',lang_front())
        ->where('status','yes')
        ->where('show_in_homePage','yes')->take(8)->get();


        $data['testi'] = Testimonial::select('name','desc','img','position')
        ->where('status','yes')
        ->where('language_id',lang_front())->take(5)->get();

        $data['team'] = Team::select('name','desc','img','position','facebook','twitter','instagram','linkedin')
        ->where('status','yes')
        ->where('language_id',lang_front())->take(4)->get();


        
        return view('front.index')->with($data);
    }



    // base function for admin 
    public function about()
    {

        $data['team'] = Team::select('name','desc','img','position','facebook','twitter','instagram','linkedin')
        ->where('status','yes')
        ->where('language_id',lang_front())->take(4)->get();
        
        return view('front.static.about')->with($data);
    }

}
