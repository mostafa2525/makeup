<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\TestiRequest;
use App\Http\Controllers\Controller;
use App\Models\Testimonial;

use Image;
use Storage;
use App\Helpers\Classes\UploadClass;

class TestiController extends Controller
{
    public function index()
    {

    	$data['testi'] = Testimonial::select('id','name','language_id','status','sort','img','desc','position')
    	->where('language_id',language())
    	->orderBy('sort')
    	->paginate(10);
    	return view('admin.testi.index')->with($data);
    }



    // return view of adding data 
    public function add()
    {
    	return view('admin.testi.add');
    }





    // storing data in db 
    public function store(TestiRequest $request)
    {
    	$data = $request->validated();
    	$data['language_id'] = language();

        // upload image of this module
        if($request->hasFile('img'))
        {
            // UPLOADS_PATH.TESTI_PATH 
            // -> static path ( you can change it from helpers/files/basehelper)

            $img = UploadClass::uploadOneImage($request,'img',UPLOADS_PATH.TESTI_PATH,80,80);
            //$img = UploadClass::uploadFile($request,'img',UPLOADS_PATH.TESTI_PATH,$all=true,$big=1340);
            $data['img'] = $img;
        }
    	Testimonial::create($data); // inserting data 
    	session()->flash('message',trans('site.added_success'));
    	return redirect(route('admin.get.testi.index'));
    }







    // return view of editing data 
    public function edit($id)
    {
    	$data['row'] = Testimonial::findOrFail($id);
    	return view('admin.testi.edit')->with($data);
    }



    // storing data in db 
    public function update(TestiRequest $request)
    {
    	$data = $request->validated();

        // upload image of this module
        if($request->hasFile('img'))
        {
            $old = Testimonial::findOrFail($request->id);
            // delete old image from server
            if($old->img)
            {
                Storage::disk('public_uploads')->delete(TESTI_PATH.$old->img);
            }
            // UPLOADS_PATH.TESTI_PATH 
            // -> static path ( you can change it from helpers/files/basehelper)
            $img = UploadClass::uploadOneImage($request,'img',UPLOADS_PATH.TESTI_PATH,80,80);
            $data['img'] = $img;
        }
    	// updating data in db
    	Testimonial::where('id', $request->id)->update($data);
    	session()->flash('message',trans('site.updated_success'));
    	return redirect(route('admin.get.testi.index'));
    }



















    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
        $old = Testimonial::findOrFail($id);
        // delete old image from server
        if($old->img)
        {
            Storage::disk('public_uploads')->delete(TESTI_PATH.$old->img);
        }
        
        
        Testimonial::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return back();

    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
            $old = Testimonial::findOrFail($value);
            // delete old image from server
            if($old->img)
            {
                Storage::disk('public_uploads')->delete(TESTI_PATH.$old->img);
            }

            Testimonial::findOrFail($value)->delete();
	    	
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return back();
    }


     // delete mutli row from table 
    public function sort(Request $request)
    {
    	$i = 1;
    	foreach ($request->sort as  $value) 
    	{
    		$data = Testimonial::find($value);
    		$data->sort = $i;
    		$data->save();
    		$i++;
    	}
    	session()->flash('message',trans('site.sorted_success'));
	    return back();
    }






    // visibility  for this item ( active or not active  -- change status of this item )
    public function visibility($id)
    {
    	$data = Testimonial::findOrFail($id);

    	switch ($data->status) 
    	{
    		case 'yes':
    			$data->status = 'no';
    			$data->save();
    			session()->flash('message',trans('site.deactivate_message'));
    			break;

    		case 'no':
    			$data->status = 'yes';
    			session()->flash('message',trans('site.activate_message'));
    			$data->save();
    			break;
    		
    		default:
    			# code...
    			break;
    	}

    	return back();

    }















   // return view of seo data 
    public function seo($id)
    {
        $data['seo'] = Slider::where('language_id',language())->findOrFail($id);
        $data['seoData'] = json_decode($data['seo']['seo']);
        return view('admin.testi.seo')->with($data);

    }



     // storing data in db 
    public function updateSeo(Request $request)
    {
        $data = $request->except('id','_token');
        // updating data in db
        $seoData = Slider::find($request->id);
        $seoData->seo = json_encode($data);
        $seoData->save();
        session()->flash('message',trans('site.updated_success'));
        return back();
    }











}
