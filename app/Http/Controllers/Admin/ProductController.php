<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;


use Image;
use Storage;
use App\Helpers\Classes\UploadClass;

class ProductController extends Controller
{



    public function index()
    {
    	$data['product'] = Product::select('id','name','language_id','status','sort','category_id','price')
    	->where('language_id',language())
    	->orderBy('sort')
    	->paginate(30);
    	return view('admin.product.index')->with($data);
    }



    // return view of adding data 
    public function add()
    {
        $data['categories'] = Category::select('id','name','language_id')
        ->where('language_id',adminAuth()->user()->language_id)
        ->orderBy('sort')
        ->get();
    	return view('admin.product.add')->with($data);
    }





    // storing data in db 
    public function store(ProductRequest $request)
    {
    	$data = $request->validated();
    	$data['language_id'] = language();

        // upload image of this module
        if($request->hasFile('img'))
        {
            // UPLOADS_PATH.CATEGORY_PATH 
            // -> static path ( you can change it from helpers/files/basehelper)
            $img = UploadClass::uploadImage($request,'img',UPLOADS_PATH.PRODUCT_PATH,178,600,205,500);
            $data['img'] = $img;
        }
        //  slug
        if($request->slug != ''){$data['slug'] = str_slug($request->slug);}
        else{$data['slug'] = str_slug   ($request->name).uniqid();}
    	Product::create($data); // inserting data 
    	session()->flash('message',trans('site.added_success'));
    	return redirect(route('admin.get.product.index'));
    }







    // return view of editing data 
    public function edit($id)
    {
        $data['categories'] = Category::select('id','name','language_id')
        ->where('language_id',adminAuth()->user()->language_id)
        ->orderBy('sort')
        ->get();
    	$data['row'] = Product::findOrFail($id);
    	return view('admin.product.edit')->with($data);
    }



    // storing data in db 
    public function update(ProductRequest $request)
    {
    	$data = $request->validated();

        // upload image of this module
        if($request->hasFile('img'))
        {
            $old = Product::findOrFail($request->id);
            // delete old image from server
            if($old->img)
            {
                Storage::disk('public_uploads')->delete(PRODUCT_PATH.$old->img);
                Storage::disk('public_uploads')->delete(PRODUCT_PATH.'small/'.$old->img);
                Storage::disk('public_uploads')->delete(PRODUCT_PATH.'medium/'.$old->img);
            }
            // UPLOADS_PATH.CATEGORY_PATH 
            // -> static path ( you can change it from helpers/files/basehelper)
            $img = UploadClass::uploadImage($request,'img',UPLOADS_PATH.PRODUCT_PATH,178,600,205,500);
            $data['img'] = $img;
        }
        //  slug
        if($request->slug != ''){$data['slug'] = str_slug($request->slug);}
        else{$data['slug'] = str_slug($request->name).uniqid();}
    	// updating data in db
    	Product::where('id', $request->id)->update($data);
    	session()->flash('message',trans('site.updated_success'));
    	return redirect(route('admin.get.product.index'));
    }



















    // deleteing row from db  ( soft delete )
    public function delete($id)
    {

    	$old = Product::findOrFail($id);

        Storage::disk('public_uploads')->delete(PRODUCT_PATH.$old->img);
        Storage::disk('public_uploads')->delete(PRODUCT_PATH.'small/'.$old->img);
        Storage::disk('public_uploads')->delete(PRODUCT_PATH.'medium/'.$old->img);


    	Product::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return back();

    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{

            $old = Product::findOrFail($value);
            Storage::disk('public_uploads')->delete(PRODUCT_PATH.$old->img);
            Storage::disk('public_uploads')->delete(PRODUCT_PATH.'small/'.$old->img);
            Storage::disk('public_uploads')->delete(PRODUCT_PATH.'medium/'.$old->img);
    		Product::findOrFail($value)->delete();
	    	
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return back();
    }


     // delete mutli row from table 
    public function sort(Request $request)
    {
    	$i = 1;
    	foreach ($request->sort as  $value) 
    	{
    		$data = Product::find($value);
    		$data->sort = $i;
    		$data->save();
    		$i++;
    	}
    	session()->flash('message',trans('site.sorted_success'));
	    return back();
    }






    // visibility  for this item ( active or not active  -- change status of this item )
    public function visibility($id)
    {
    	$data = Product::findOrFail($id);

    	switch ($data->status) 
    	{
    		case 'yes':
    			$data->status = 'no';
    			$data->save();
    			session()->flash('message',trans('site.deactivate_message'));
    			break;

    		case 'no':
    			$data->status = 'yes';
    			session()->flash('message',trans('site.activate_message'));
    			$data->save();
    			break;
    		
    		default:
    			# code...
    			break;
    	}

    	return back();

    }















   // return view of seo data 
    public function seo($id)
    {
        $data['seo'] = Product::where('language_id',language())->findOrFail($id);
        $data['seoData'] = json_decode($data['seo']['seo']);
        return view('admin.product.seo')->with($data);

    }



     // storing data in db 
    public function updateSeo(Request $request)
    {
        $data = $request->except('id','_token');
        // updating data in db
        $seoData = Product::find($request->id);
        $seoData->seo = json_encode($data);
        $seoData->save();
        session()->flash('message',trans('site.updated_success'));
        return back();
    }











}
