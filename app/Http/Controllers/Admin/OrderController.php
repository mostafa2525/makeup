<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;


class OrderController extends Controller
{



    public function services()
    {

    	$data['messages'] = Order::where('type','services')->latest()->paginate(30);
    	return view('admin.order.services')->with($data);
    }



    public function products()
    {

        $data['messages'] = Order::where('type','product')->latest()->paginate(30);
        return view('admin.order.products')->with($data);
    }




    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
    	Order::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return redirect(route('admin.get.order.index'));

    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
    		Order::findOrFail($value)->delete();
	    	
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return redirect(route('admin.get.order.index'));
    }








}
