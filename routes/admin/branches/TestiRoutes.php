<?php 

//  Country routes
//  
Route::group(['prefix'=>'testi'],function()
{
	
	// show all data an data table 
	Route::get('/all','TestiController@index')->name('get.testi.index'); 
	// view form for adding new item 
	Route::get('/add','TestiController@add')->name('get.testi.add');  
	// store data of item 
	Route::post('/store','TestiController@store')->name('post.testi.store'); 
	// view data of specific item  
	Route::get('/edit/{id}','TestiController@edit')->name('get.testi.edit'); 
	// update data of specific item 
	Route::put('/update','TestiController@update')->name('put.testi.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','TestiController@delete')->name('get.testi.delete'); 
	// delete multi  item
	Route::post('/delete/multi','TestiController@deleteMulti')->name('post.testi.deleteMulti'); 

	// sort elements
	Route::post('/sort','TestiController@sort')->name('post.testi.sort'); 
	// make this item visibile or not  
	Route::get('/visibility/{id}','TestiController@visibility')->name('get.testi.visibility'); 

	

});