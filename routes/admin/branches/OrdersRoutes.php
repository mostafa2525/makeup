<?php 

//  Newsletter routes
//  
Route::group(['prefix'=>'order'],function()
{
	
	// show all data an data table 
	Route::get('/services','OrderController@services')->name('get.order.services'); 
	Route::get('/products','OrderController@products')->name('get.order.products'); 

	Route::get('/delete/{id}','OrderController@delete')->name('get.order.delete'); 
	// delete multi  item
	Route::post('/delete/multi','OrderController@deleteMulti')->name('post.order.deleteMulti'); 

	

});