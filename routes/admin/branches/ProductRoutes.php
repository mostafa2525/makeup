<?php 

//  categories routes
//  
Route::group(['prefix'=>'product'],function()
{
    
    

	// show all data an data table 
	Route::get('/all','ProductController@index')->name('get.product.index'); 
	// view form for adding new item 
	Route::get('/add','ProductController@add')->name('get.product.add');  
	// store data of item 
	Route::post('/store','ProductController@store')->name('post.product.store'); 
	// view data of specific item  
	Route::get('/edit/{id}','ProductController@edit')->name('get.product.edit')->where('id', '[0-9]+'); 
	// update data of specific item 
	Route::put('/update','ProductController@update')->name('put.product.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','ProductController@delete')->name('get.product.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','ProductController@deleteMulti')->name('post.product.deleteMulti'); 

	// sort elements
	Route::post('/sort','ProductController@sort')->name('post.product.sort'); 
	// make this item visibile or not  
	Route::get('/visibility/{id}','ProductController@visibility')->name('get.product.visibility')->where('id', '[0-9]+'); 




	// view data of seo
	Route::get('/seo/{id}','ProductController@seo')->name('get.product.seo')->where('id', '[0-9]+');
	// update data of seo
	Route::post('/seo/update','ProductController@updateSeo')->name('post.product.updateSeo'); 



	// more images  for Service
	Route::get('/add-images/{id}','Product\ImageController@addImages')->name('get.product.addImages')->where('id', '[0-9]+');
	Route::post('/add-images','Product\ImageController@moreImages')->name('post.product.moreImages'); 
	Route::get('/delete-image/{id}','Product\ImageController@deleteImage')->name('get.product.delete.image')->where('id', '[0-9]+');





	

});