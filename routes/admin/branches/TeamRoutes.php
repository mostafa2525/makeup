<?php 

//  Country routes
//  
Route::group(['prefix'=>'team'],function()
{
	
	// show all data an data table 
	Route::get('/all','TeamController@index')->name('get.team.index'); 
	// view form for adding new item 
	Route::get('/add','TeamController@add')->name('get.team.add');  
	// store data of item 
	Route::post('/store','TeamController@store')->name('post.team.store'); 
	// view data of specific item  
	Route::get('/edit/{id}','TeamController@edit')->name('get.team.edit'); 
	// update data of specific item 
	Route::put('/update','TeamController@update')->name('put.team.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','TeamController@delete')->name('get.team.delete'); 
	// delete multi  item
	Route::post('/delete/multi','TeamController@deleteMulti')->name('post.team.deleteMulti'); 

	// sort elements
	Route::post('/sort','TeamController@sort')->name('post.team.sort'); 
	// make this item visibile or not  
	Route::get('/visibility/{id}','TeamController@visibility')->name('get.team.visibility'); 

	

});