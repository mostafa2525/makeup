<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  prefix tgat show at url is (adminPnel)
//  prefix tgat show at url is (adminPnel)


//  for language (  package laravel macamara )
Route::group(
[
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function()
{
	Route::namespace('Front')->name('front.')->group(function()
	{

		Route::get('/','HomeController@index')->name('get.home.index')->middleware('getRoute');
		Route::get('/about','HomeController@about')->name('get.home.about')->middleware('getRoute');

		// contact us page
		Route::get('/contact-us','ContactUsController@contact')->name('get.contactus.contact')->middleware('getRoute');
		// gallery page
		Route::get('/gallery','ContactUsController@gallery')->name('get.contactus.gallery')->middleware('getRoute');

		// portfolio page
		// Route::get('/portfolio','PortfolioController@portfolio')->name('get.portfolio.index')->middleware('getRoute');


		// services 
		Route::get('/services','ServiceController@all')->name('get.service.all')->middleware('getRoute');
		Route::get('/services/search','ServiceController@search')->name('get.service.search')->middleware('getRoute');
		// show services according to category
		Route::get('/service/{slugCat}','ServiceController@category')->name('get.service.category')->middleware('getRoute');
		// show one service
		Route::get('/services/{slugService}','ServiceController@show')->name('get.service.show')->middleware('getRoute');
		// order this service
		Route::post('/services/order','ServiceController@order')->name('post.service.order');





		// products 
		Route::get('/products','ProductsController@all')->name('get.product.all')->middleware('getRoute');
		// show services according to category
		// Route::get('/products/{slugCat}','ProductsController@category')->name('get.product.category')->middleware('getRoute');
		// show one service
		Route::get('/products/{slugProduct}','ProductsController@show')->name('get.product.show')->middleware('getRoute');
		// order this product
		Route::post('/products/order','ProductsController@order')->name('post.product.order');





		// Blog 
		Route::get('/blog','BlogController@index')->name('get.blog.index')->middleware('getRoute');
		Route::get('/blog/search','BlogController@search')->name('get.blog.search')->middleware('getRoute');
		Route::get('/blog/{slugBlog}','BlogController@show')->name('get.blog.show')->middleware('getRoute');



		// subscribe
		Route::post('/subscribe','ContactUsController@subscribe')->name('post.contactus.subscribe');
		// send message from contact us page 
		Route::post('/send-message','ContactUsController@sendMessage')->name('post.contactus.sendMessage');

	});


});
