<?php 

return [

    // buttons 
    'viewMore'    			=> 'View More',
    'startService'    		=> 'Start a Service',
    'details'    			=> 'Details',
    'serviceDetails'    	=> 'Service Details',
    'projectDetails'    	=> 'Project Details',
    'usefulLinks'			=> 'Useful Links ',
    'workWithUs'			=> 'Work With Us',
    'bookNow'				=> '  Book Now  ',
    'makeAppoint'			=> '  Make an Appoinment ', 
    'submit'				=> '  Submit Now ',


    




	'dashboard' 				=> ' لوحة التحكم  ',
	'menu' 	=> [
					'home'=>'  الرئيسية ',
					'all' => 'قائمة الطعام ',
					'branches' => ' فروعنا  ',
				],


	'language' => 'اللغة  ',
	'howToGetUs' => 'كيف تتواصل معنا  ',
	'office' => ' الادارة العامة    ',
	'search' => '  بحث     ',
	'price' => '  ريال      ',
	'readMore' => '  اقرأ المزيد      ',
 


	//  messages 
	
	'added_success'			=> ' تم الاضافة بنجاح ',
	'updated_success'		=> ' تم التعديل بنجاح ',
	'deleted_success'		=> ' تم الحذف بنجاح ',
	'sorted_success'		=> ' تم ترتيب البيانات ',
	'dataNotFound'		    => ' للاسف   , لا توجد بيانات  !',
	'activate_message' 		=> ' تم التفعيل بنجاح ',
	'deactivate_message' 	=> ' تم الغاء التفعيل بنجاح ',
	'orderSuccess' 			=> ' تم ارسال طلبك بنجاح وسيتم التوصل معك  ! ',
	// 'subscribeMessage' 		=> ' شكرا لاشتراكك فى قائمتنا البريدية  ',
	'contactusMessage' 		=> ' Your Message Have Been Sent  ',
	'notFoundData'			=> ' Sorry , There Is No Any Data',
    'subscribeMessage'		=> '  Thanks For Your Subscribe ',
    'successOrderMessage'	=> '  Thanks , We Will Contact With You ',
	
    







	



];

