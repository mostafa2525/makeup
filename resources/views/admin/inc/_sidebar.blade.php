                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">

                    <div class="page-sidebar navbar-collapse collapse">
         
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                             
                            </li>
                            <li class="nav-item start {{ sidebar_base('')  }} ">
                                <a href="{{ route('admin.get.home.index') }}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">@lang('site.dashboard')</span>
                              
                                </a>
                            </li>
                            <li class="heading">
                                <h3 class="uppercase"> @lang('site.content') </h3>
                            </li>


                            <li class="nav-item {{ sidebar_base('settings')  }}  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-cogs"></i>
                                    <span class="title">@lang('site.settings')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('settings','base')  }}">
                                        <a href="{{ route('admin.get.settings.base') }}" class="nav-link ">
                                            <span class="title">@lang('site.base')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('settings','seo')  }} ">
                                        <a href="{{ route('admin.get.settings.seo') }}" class="nav-link ">
                                            <span class="title">@lang('site.seo')</span>
                                        </a>
                                    </li>

                                    <li class="nav-item {{ sidebar_base('settings','site')  }} ">
                                        <a href="{{ route('admin.get.settings.siteContent') }}" class="nav-link ">
                                            <span class="title">@lang('site.siteContent')</span>
                                        </a>
                                    </li>

                                    <li class="nav-item {{ sidebar_base('settings','aboutus')  }} ">
                                        <a href="{{ route('admin.get.settings.aboutus') }}" class="nav-link ">
                                            <span class="title">@lang('site.aboutUsPage')</span>
                                        </a>
                                    </li>


                               
                                </ul>
                            </li>






                            <li class="nav-item {{ sidebar_base('slider')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-sliders"></i>
                                    <span class="title">@lang('site.slider')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('slider','add')  }}">
                                        <a href="{{ route('admin.get.slider.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('slider','all')  }} ">
                                        <a href="{{ route('admin.get.slider.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>



                            <li class="nav-item {{ sidebar_base('category')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-bars"></i>
                                    <span class="title">@lang('site.categories')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('category','add')  }}">
                                        <a href="{{ route('admin.get.category.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('category','all')  }} ">
                                        <a href="{{ route('admin.get.category.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>



                             <li class="nav-item {{ sidebar_base('service')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-diamond"></i>
                                    <span class="title">@lang('site.services')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('service','add')  }}">
                                        <a href="{{ route('admin.get.service.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('service','all')  }} ">
                                        <a href="{{ route('admin.get.service.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>



                            <li class="nav-item {{ sidebar_base('product')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-cart-arrow-down"></i>
                                    <span class="title">@lang('site.products')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('product','add')  }}">
                                        <a href="{{ route('admin.get.product.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('product','all')  }} ">
                                        <a href="{{ route('admin.get.product.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>


                            <li class="nav-item {{ sidebar_base('order') }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-sort"></i>
                                    <span class="title">@lang('site.orders')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('order','products')  }} ">
                                        <a href="{{ route('admin.get.order.products') }}" class="nav-link ">
                                            <span class="title">@lang('site.orderProducts')</span>
                                        </a>
                                    </li>

                                    <li class="nav-item {{ sidebar_base('order','services')  }} ">
                                        <a href="{{ route('admin.get.order.services') }}" class="nav-link ">
                                            <span class="title">@lang('site.orderServices')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>


                            
                             <li class="nav-item {{ sidebar_base('testi')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-bullseye"></i>
                                    <span class="title">@lang('site.testi')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('testi','add')  }}">
                                        <a href="{{ route('admin.get.testi.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('testi','all')  }} ">
                                        <a href="{{ route('admin.get.testi.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>



                            <li class="nav-item {{ sidebar_base('team')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-users"></i>
                                    <span class="title">@lang('site.team')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('team','add')  }}">
                                        <a href="{{ route('admin.get.team.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('team','all')  }} ">
                                        <a href="{{ route('admin.get.team.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>





                             <li class="nav-item {{ sidebar_base('brand')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-bullseye"></i>
                                    <span class="title">@lang('site.brand')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('brand','add')  }}">
                                        <a href="{{ route('admin.get.brand.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('brand','all')  }} ">
                                        <a href="{{ route('admin.get.brand.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>





                             <li class="nav-item {{ sidebar_base('blog')  }} {{ sidebar_base('questionPage')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-rss"></i>
                                    <span class="title">@lang('site.blog')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('blog','add')  }}">
                                        <a href="{{ route('admin.get.blog.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('blog','all')  }} ">
                                        <a href="{{ route('admin.get.blog.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>




                            <li class="nav-item {{ sidebar_base('staticPage')  }} {{ sidebar_base('questionPage')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-square"></i>
                                    <span class="title">@lang('site.staticPage')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('staticPage','add')  }}">
                                        <a href="{{ route('admin.get.staticPage.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('staticPage','all')  }} ">
                                        <a href="{{ route('admin.get.staticPage.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>



                            <li class="nav-item {{ sidebar_base('newsletter') }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="title">@lang('site.newsletter')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('newsletter','all')  }} ">
                                        <a href="{{ route('admin.get.newsletter.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>


                            <li class="nav-item {{ sidebar_base('message') }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="title">@lang('site.messages')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('message','all')  }} ">
                                        <a href="{{ route('admin.get.message.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>






                             <li class="nav-item {{ sidebar_base('admin')  }} ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-users"></i>
                                    <span class="title">@lang('site.admins')</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ sidebar_base('admin','add')  }}">
                                        <a href="{{ route('admin.get.admin.add') }}" class="nav-link ">
                                            <span class="title">@lang('site.add')</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ sidebar_base('admin','all')  }} ">
                                        <a href="{{ route('admin.get.admin.index') }}" class="nav-link ">
                                            <span class="title">@lang('site.view')</span>
                                        </a>
                                    </li>
                               
                                </ul>
                            </li>









                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->


                 <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">
                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content">
                            <!-- BEGIN PAGE HEADER-->
                            <!-- BEGIN THEME PANEL -->
                          
                            <!-- END THEME PANEL -->
