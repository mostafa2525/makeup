@extends('front.main')


@section('content')




    <!-- Start: Breadcrumb Area
    ============================= -->

    <section id="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Blog</h2>
                    <ul class="breadcrumb-nav list-inline">
                        <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
                        <li>Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Breadcrumb Area
    ============================= -->


    <!-- Start: Blog Full Width
    ============================= -->
    <section id="blog-content" class="full-width section-padding">
        <div class="container-fluid">
            <div class="row full-width" id="grid">

            	@foreach($posts as $post)
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <article class="blog-post">
                        <div class="post-thumb">
                        	<a href="{{route('front.get.blog.show',[$post->slug])}}">
                            <img src="{{getImage(BLOG_PATH.'small/'.$post->img)}}" alt="{{$post->name}}">
                        	</a>
                        </div>

                        <div class="post-content">
                            <ul class="meta-info">
                                <li class="post-date"><a href="{{route('front.get.blog.show',[$post->slug])}}"> {{date('Y-M-D',strtotime($post->created_at))}} </a></li>
                                <li class="posted-by"><a href="{{route('front.get.blog.show',[$post->slug])}}">by {{$setting->site_name}}</a></li>
                            </ul>
                            <h4 class="post-title"><a href="{{route('front.get.blog.show',[$post->slug])}}">{{\Str::Words($post->name,10,'...')}}</a></h4>
                            <p class="content">
                               {{\Str::Words($post->small_description,20,'...')}}
                            </p>
                            <a href="{{route('front.get.blog.show',[$post->slug])}}" class="read-more">Read More <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </article>  
                </div>  
                @endforeach

                <div class="col-sm-12">
                    {{$posts->links()}}
                </div>

                @if(!$posts->count())
                <div class="col-sm-12">
                    <h4 class="alert alert-danger text-center">@lang('frontSite.notFoundData')</h4>
                </div>
                @endif



        

            </div>
        </div>
    </section>

    <!-- End: Blog Full Width
    ============================= -->


@endsection