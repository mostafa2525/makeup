@extends('front.main')


@section('content')



    <!-- Start: Breadcrumb Area
    ============================= -->

    <section id="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Blog</h2>
                    <ul class="breadcrumb-nav list-inline">
                        <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
                        <li><a href="{{ route('front.get.blog.index') }}">Blog</a></li>
                        <li>{{$row->name}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Breadcrumb Area
    ============================= -->


    <!-- Start: Blog right Sidebar
    ============================= -->
        <section id="blog-content" class="section-padding single-post">
        <div class="container">

            <div class="row">
                <!-- Blog Content -->
                <div class="col-lg-8 col-md-12 mb-5 mb-lg-0">
                    <article class="blog-post">                        
                        <div class="post-thumb">
                            <img src="{{getImage(BLOG_PATH.$row->img)}}" alt="{{$row->name}}">
                        </div>
                        <div class="post-content">
                        <div class="post-header">
                            <h3 class="post-title">{{$row->name}}</h3>
                            <ul class="meta-info text-left">
                                <li class="post-date"> <a> {{date('Y-M-D',strtotime($row->created_at))}}</a></li>
                            </ul>
                   
                        </div>
                            <h5 class="pb-3 pt-4">{{$row->small_description}}</h5>
                            <p>
                              {!!$row->description!!}
                            	
                            </p>
                        </div>
                        
                        <div class="meta-data">
                            <ul>
                            	<?php $tags = explode('-', $row->tags); ?>
                                <li class="pr-1">Tags:</li>

                            	@foreach($tags as $tag)
                                <li><a href="">{{$tag}}</a></li>
                                @endforeach
                            </ul>
                            <ul class="text-right share">
                                <li class="pr-1">Share:</li>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fab fa-envelope"></i></a></li>
                            </ul>
                        </div>

                        <hr>

                     
                    </article>
                </div>

                <!-- Sidebar -->
                <div class="col-lg-4 col-md-12">
                    <section class="sidebar">
                        
                        <aside class="widget widget-search">
                            <h5 class="widget-title"><img src="{{furl()}}/img/section-icon.png" alt="">Search</h5>
                            <form class="search-form" action="{{route('front.get.blog.search')}}" method="GET">
                                <span class="input input--hantus">
                                    <input class="input__field input__field--hantus" type="text" id="input-01" 
                                    name="search" />
                                    <label class="input__label input__label--hantus" for="input-01">
                                        <svg class="graphic graphic--hantus" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                        <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                                        </svg>
                                        <span class="input__label-content input__label-content--hantus">Search blog</span>
                                    </label>
                                </span>
                                <input type="submit" class="search-btn searchBtn" value="Go" style="">
                            </form>
                        </aside>

                        <aside class="widget widdget-recent-post">
                            <h5 class="widget-title"><img src="{{furl()}}/img/section-icon.png" alt="">Recent News</h5>

                            @foreach($posts as $post)
                            <div class="recent-post">
                                <a href="{{route('front.get.blog.show',[$post->slug])}}">
                                	<h6>
                                		{{\Str::Words($post->name,10,'...')}}
                                	</h6>
                                </a>
                                <p>{{date('Y-M-D',strtotime($post->created_at))}}</p>
                            </div>
                            @endforeach
                            
                        </aside>


                        <aside class="widget widget-tag">
                            <h5 class="widget-title"><img src="{{furl()}}/img/section-icon.png" alt="">Tags</h5>
                            <ul class="tags">
                            	@foreach($tags as $tag)
                                <li><a href="">{{$tag}}</a></li>
                                @endforeach
                            </ul>
                        </aside>


                        

                    </section>
                </div>
            </div>

        </div>
    </section>

    <!-- End: Blog right Sidebar
    ============================= -->





@endsection