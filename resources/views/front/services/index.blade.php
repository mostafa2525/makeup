@extends('front.main')


@section('content')



    <!-- Start: Breadcrumb Area
    ============================= -->

    <section id="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Services</h2>
                    <ul class="breadcrumb-nav list-inline">
                        <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
                        <li>Services</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Breadcrumb Area
    ============================= -->

    <!-- Start: Our Service
    ============================= -->
    <section id="services" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <h2>Our Services</h2>
                        <hr>
                        <p>These are the services we provide, these makes us stand apart.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($allServices as $ser)
                <div class="col-lg-3 col-md-6 col-sm-6 mb-5 mb-lg-0">                    
                    <div class="service-box text-center" style="    margin-bottom: 25px;">                        
                        <figure>
                            <img src="{{ getImage(SERVICE_PATH.'small/'.$ser->img)}}" alt="">
                            <figcaption>
                                <div class="inner-text">
                                    <a href="{{route('front.get.service.show',[$ser->slug])}}" class="boxed-btn">@lang('frontSite.bookNow')</a>
                                </div>
                            </figcaption>
                        </figure>
                        <h4>{{\Str::Words($ser->name,3,'')}}</h4>
                        <p>{{\Str::Words($ser->small_description,13,'')}}</p>
                        <p class="price">$ {{$ser->price}}</p>
                    </div>
                </div>
                @endforeach
                <div class="col-sm-12">                    
                    <div class="service-box text-center">                        
                       {{$allServices->links()}}
                    </div>
                </div>


                @if(!$allServices->count())
                <div class="col-sm-12">
                    <h4 class="alert alert-danger text-center">@lang('frontSite.notFoundData')</h4>
                </div>
                @endif

              
            </div>
        </div>
    </section>

    <!-- End:  Our Service
    ============================= -->



@endsection