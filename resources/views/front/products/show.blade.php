@extends('front.main')


@section('content')


 <!-- Start: Our Pricing Plan
    ============================= -->
    
    <section id="pricing" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                 
                </div>
            </div>

            <div class="row">
                
                <div class="col-lg-12">
                    <div class="pricing-box text-center" style="background: url({{furl()}}/img/pricing/pricing01.jpg) no-repeat center / cover; border:2px solid #222;">
                        <h3>{{$row->name}}</h3>
                        <hr>
                        <h3>  <img src="{{getImage(PRODUCT_PATH.'small/'.$row->img)}}" class="img-thumbnail"> </h3>
                        <hr>
                        
                        <div class="price"><sup>$</sup> <span>{{$row->price}}</span></div>
                        <ul class="pricing-content">
                            <p>
                            	{!!$row->description!!}
                            </p>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Our Pricing Plan
    ============================= -->



        <!-- Start: Appoinment
    ============================= -->
    
    <section id="appoinment" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="opening-hours">
                        <h3>Opening Hours</h3>
                        <p>A collection of textile samples lay spread out on the table Samsa was a travelling salesman.</p>
                        <ul>
                            <li>Monday :    8:00am - 21:00pm</li>
                            <li>Monday :    8:00am - 21:00pm</li>
                            <li>Monday :    8:00am - 21:00pm</li>
                            <li>Monday :    8:00am - 21:00pm</li>
                            <li>Sunday:     Close</li>
                        </ul>
                    </div>
                    <div class="appoinment-wrapper">
                        <form  id="order-form">
                            <div class="">
		                        <h3>Ask For This Product</h3>
		                        <br>
		                        <br>
		                    </div>

                            @csrf
                            <div class="col-md-12">
                                <ul id="errors"></ul>
                                <input type="hidden" name="product_id" value="{{$row->id}}">
                                
                            </div>
                            <span class="input input--hantus">
                                <input required placeholder="Name" class="input__field input__field--hantus" type="text" id="input-08" name="name" />
                              
                            </span>


                            <span class="input input--hantus">
                                <input required placeholder="Email" name="email" class="input__field input__field--hantus" type="email" id="input-011"  />
                         
                            </span>


                            <span class="input input--hantus">
                                <input placeholder="Phone" name="phone" class="input__field input__field--hantus" type="number" id="input-09"  />
                          
                            </span>
        
                            <span class="input input--hantus textarea">
                                <textarea required   placeholder="Message" name="message" class="input__field input__field--hantus" rows="5" id="input-10"></textarea>
                            
                            </span>

                            <button type="submit" class="boxed-btn">@lang('frontSite.submit')</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Appoinment
    ============================= -->

@endsection


@section('script')

<script type="text/javascript">
    $('#order-form').parsley();
</script>
<script type="text/javascript">
  


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#order-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#order-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.product.order')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.input--hantus').find("input,textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection
