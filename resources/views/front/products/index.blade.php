@extends('front.main')


@section('content')



    <!-- Start: Breadcrumb Area
    ============================= -->

    <section id="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Products</h2>
                    <ul class="breadcrumb-nav list-inline">
                        <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
                        <li>Products</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Breadcrumb Area
    ============================= -->





      <!-- Start: Pricing
    ============================= -->
    
    <section id="portfolio" class="section-padding pricing-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <h2>Pricing</h2>
                        <hr>
                        <p>You can judge my work by the portfolio we have done</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
             
                    <div class="tab-content">
                       
                        <div id="massage" class="tab-pane fade-in active">
                            <ul>
                                @foreach($allProducts as $prod)
                                <li>
                                    <a href="{{route('front.get.product.show',[$prod->slug])}}">
                                    <img src="{{getImage(PRODUCT_PATH.'small/'.$prod->img)}}" alt="{{$prod->name}}">
                                    </a>
                                    <a href="{{route('front.get.product.show',[$prod->slug])}}"><h4>{{$prod->name}} <span class="price">${{$prod->price}}</span></h4></a>
                                    <p>{{\Str::Words($prod->small_description,'6','...')}}</p>
                                </li>
                                @endforeach
                          
                            </ul>
                        </div>
                 
                    </div>
                </div>
            </div>

            
        </div>
    </section>

    <!-- End: Pricing
    ============================= -->



@endsection