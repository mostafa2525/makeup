   
   </head>

<body>
    
    <!-- Start: Preloader
    ============================= -->

    <div class="preloader">        
        <div class="wrapper">
            <div class="circle circle-1"></div>
            <div class="circle circle-1a"></div>
            <div class="circle circle-2"></div>
            <div class="circle circle-3"></div>
        </div>
        <h1>Loading&hellip;</h1>
    </div>

    <!-- End: Preloader
    ============================= -->

    <!-- Start: Header Top
    ============================= -->
    <section id="header-top">
        <div class="container">
            
            <div class="row">
                <div class="col-lg-6 col-md-6 text-center text-md-left">
                    <p><i class="fas fa-clock"></i>Opening Hours - 10 Am to 6 PM</p>
                    <ul class="header-social d-inline-block">
                        @if($setting->facebook)
                        <li><a href="{{$setting->facebook}}"  target="_blank"><i class="fab fa-facebook-f" ></i></a></li>
                        @endif
                         @if($setting->twitter)
                        <li><a href="{{$setting->twitter}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        @endif
                         @if($setting->instagram)
                        <li><a href="{{$setting->instagram}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        @endif
                         @if($setting->linkedin)
                        <li><a href="{{$setting->linkedin}}" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        @endif
  
                    </ul>
                    
                </div>
                <div class="col-lg-6 col-md-6 text-center text-md-right header-top-right">
                    <ul>
                        <li><a href="#"><i class="fas fa-envelope"></i>{{$setting->email}}</a></li>
                        <li><a href="#"><i class="fas fa-phone"></i>{{$setting->mobile}}</a></li>
                    </ul>       
                </div>
            </div>
        </div>
    </section>
    <!-- End: Header Top
    ============================= -->