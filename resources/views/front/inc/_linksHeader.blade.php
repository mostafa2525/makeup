<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from nayrathemes.com/demo/html/hantus/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Apr 2019 19:07:10 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="keywords" content="Hantus, Responsive, SPA Template, Bootstrap 4,">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ furl()}}/img/favicon.png" type="image/x-icon" />
    <title>Beauty @yield('title')</title>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ furl()}}/css/style.css">
    <link rel="stylesheet" href="{{ furl()}}/css/responsive.css">
    <link rel="stylesheet" href="{{ furl()}}/seoera/css/ltr.css">

    @yield('style')


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->