    
    <!-- Scripts -->
    <script src="{{ furl()}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{ furl()}}/js/popper.min.js"></script>
    <script src="{{ furl()}}/js/bootstrap.min.js"></script>
    <script src="{{ furl()}}/js/jquery.sticky.js"></script>
    <script src="{{ furl()}}/js/owl.carousel.min.js"></script>
    <script src="{{ furl()}}/js/jquery.shuffle.min.js"></script>
    <script src="{{ furl()}}/js/jquery.counterup.min.js"></script>
    <script src="{{ furl()}}/js/wow.min.js"></script>
    <script src="{{ furl()}}/js/jquery.meanmenu.min.js"></script>
    <script src="{{ furl()}}/js/jquery.magnific-popup.min.js"></script>
    
    <!-- Custom Script -->
    <script src="{{ furl()}}/js/custom.js"></script>


<script type="text/javascript" src="{{furl()}}/seoera/parsley.js"></script>
@if(Request::segment(1) == 'ar')
<script type="text/javascript" src="{{furl()}}/seoera/i18n/ar.js"></script>
@else
<script type="text/javascript" src="{{furl()}}/seoera/i18n/en.js"></script>
@endif
<script type="text/javascript" src="{{furl()}}/seoera/toster/jquery.toast.min.js"></script>


    @if(session('message') !=  null )
    <script type="text/javascript">
        $.toast({
                    heading: 'Message',
                    text: "{{ session('message') }}",
                    position: 'top-right',
                    stack: false,
                    icon: 'info',
                    hideAfter: 10000 
                })
    </script>
    @endif

    @yield('script')

    <script type="text/javascript">
  


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#subscribe-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#subscribe-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.contactus.subscribe')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
             $("#errors-footer").html('');
             $("#errors-footer").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.input--hantus').find("input,textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors-footer").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors-footer").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>





    
</body>


</html>