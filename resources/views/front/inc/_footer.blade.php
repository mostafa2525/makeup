
   
    <!-- Start: Subscribe
    ============================= -->
    <section id="subscribe">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 text-lg-left text-center mb-lg-0 mb-3">
                    <i class="ei ei-icon_mail"></i>
                    <h3>SIGN UP FOR NEWS AND OFFRERS</h3>
                    <p>Subcribe to lastest smartphones news & great deals we offer</p>
                </div>
                <div class="col-lg-5 col-md-12 text-center">
                    <form id="subscribe-form" action="#" method="POST">
                        <input required type="email" name="email" id="subscribe-mail" placeholder="Email" required>
                        <button type="submit">Subscribe</button>
                        @csrf
                        
                    </form>
                    <div class="" style="margin-top:70px;">
                        <ul id="errors-footer"></ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End: Subscribe
    ============================= -->
    
    
    
    <!-- Start: Footer Sidebar
    ============================= -->
    <footer id="footer-widgets">
        <div class="container">

            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 mb-lg-0 mb-4">
                    <aside class="widget widget_about">
                        <div class="footer-logo"><img src="{{ furl()}}/img/logo.png" alt=""></div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.</p>
                        <ul class="widget-info">
                            <li><i class="fas fa-map-marker"></i>{{$setting->address}}</li>
                            <li><i class="fas fa-phone"></i>{{$setting->mobile}}</li>
                            <li><i class="fas fa-envelope"></i>{{$setting->email}}</li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb-lg-0 mb-md-0 mb-4">
                    <aside class="widget widget_recent">
                        <h4 class="widget-title"><img src="{{ furl()}}/img/section-icon.png" alt="">Latest News</h4>
                        <ul>

                            @foreach($footerPosts as $pos)
                            <li class="latest-news">
                                <a href="{{route('front.get.blog.show',[$pos->slug])}}">
                                    <h6> {{\Str::Words($pos->name,10,'...')}}</h6>
                                    <p>{{date('Y-M-D',strtotime($pos->created_at))}}</p>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb-lg-0 mb-4">
                    <aside class="widget widget_links">
                        <h4 class="widget-title"><img src="{{ furl()}}/img/section-icon.png" alt="">Quick Link</h4>
                        <div class="row">
                            <ul class="col-sm-6">
                                <li><a href="{{route('front.get.home.index')}}">Home</a></li>
                                <li><a href="{{route('front.get.home.about')}}">About</a></li>
                                <li><a href="{{route('front.get.contactus.contact')}}">Contact</a></li>
                                <li><a href="{{route('front.get.service.all')}}">Service</a></li>
                                <li><a href="{{route('front.get.product.all')}}">Products</a></li>
                                <li><a href="{{route('front.get.blog.index')}}">Blog</a></li>
                            </ul>
                            <ul class="col-sm-6">
                                @foreach($services as $ser)
                                <li>
                                    <a href="{{route('front.get.service.show',[$ser->slug])}}">
                                        {{\Str::Words($ser->name,3,'')}}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        
                        
                    </aside>
                </div>
               <!--  <div class="col-lg-3 col-md-6 col-sm-6">
                    <aside class="widget widdget-instagram">
                        <h4 class="widget-title"><img src="{{ furl()}}/img/section-icon.png" alt="">Instagram</h4>
                        <ul class="instagram-photos">                                
                            <li>
                                <img src="{{ furl()}}/img/instagram/instagram01.jpg" alt="">
                                <div class="instagram-overlay">
                                    <a href="#">+</a>
                                </div>
                            </li>                                
                            <li>
                                <img src="{{ furl()}}/img/instagram/instagram02.jpg" alt="">
                                <div class="instagram-overlay">
                                    <a href="#">+</a>
                                </div>
                                
                            </li>                                
                            <li>
                                <img src="{{ furl()}}/img/instagram/instagram03.jpg" alt="">
                                <div class="instagram-overlay">
                                    <a href="#">+</a>
                                </div>
                            </li>                               
                            <li>
                                <img src="{{ furl()}}/img/instagram/instagram04.jpg" alt="">
                                <div class="instagram-overlay">
                                    <a href="#">+</a>
                                </div>
                            </li>                                
                            <li>
                                <img src="{{ furl()}}/img/instagram/instagram05.jpg" alt="">
                                <div class="instagram-overlay">
                                    <a href="#">+</a>
                                </div>
                                
                            </li>                                
                            <li>
                                <img src="{{ furl()}}/img/instagram/instagram06.jpg" alt="">
                                <div class="instagram-overlay">
                                    <a href="#">+</a>
                                </div>
                            </li>
                        </ul>
                    </aside>
                </div> -->
            </div>

        </div>
    </footer>
    <!-- End: Footer Sidebar
    ============================= -->

    <!-- Start: Footer Copyright
    ============================= -->

    <section id="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12 text-lg-left text-center mb-lg-0 mb-3 copyright-text">
                    <ul>
                        <li><a href="#">&copy; eraasoft </a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
              
            </div>
        </div>
    </section>

    <!-- End: Footer Copyright
    ============================= -->