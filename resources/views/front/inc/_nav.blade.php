   <!-- Start: Navigation
    ============================= -->
    <section class="navbar-wrapper">
        <div class="navbar-area sticky-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-6">
                        <div class="logo main">
                            <a href="{{route('front.get.home.index')}}"><img class="responsive" src="{{ furl()}}/img/logo.png" alt="Startkit"></a>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 d-none d-lg-block text-right">
                        <nav class="main-menu">
                            <ul>
                                <li class="{{active_link('')}}">
                                    <a href="{{route('front.get.home.index')}}">Home</a>
                                </li>
                                <li class="{{active_link('products')}}">
                                    <a href="{{route('front.get.product.all')}}">Products</a>
                                </li>
                                <li class="c-dropdowns {{active_link('services')}}">
                                    <a href="{{route('front.get.service.all')}}">Services</a>
                                    <ul class="cr-dropdown-menu">
                                        @foreach($services as $ser)
                                        <li>
                                            <a href="{{route('front.get.service.show',[$ser->slug])}}">
                                                {{\Str::Words($ser->name,3,'')}}
                                            </a>
                                        </li>
                                        @endforeach
                                       
                                    </ul>
                                </li>
                              

                                <li class="{{active_link('blog')}}">
                                    <a href="{{route('front.get.blog.index')}}">Blog</a>
                                </li>
                                <li class="{{active_link('about')}}">
                                    <a href="{{route('front.get.home.about')}}">About</a>
                                </li>
                                <li class="{{active_link('contact-us')}}">
                                    <a href="{{route('front.get.contactus.contact')}}">Contact</a>
                                </li>
                                
                                <li class="search-button">
                                    <div id="sb-search" class="sb-search " >
                                        <form method="get" action="{{route('front.get.service.search')}}">
                                            <input class="sb-search-input " onkeyup="buttonUp();" placeholder="Search"  type="search" value="" name="search" id="search" >
                                            <input class="sb-search-submit" type="submit"  value="">
                                            <span class="sb-icon-search"><i class="ei ei-search"></i></span>
                                        </form>
                                    </div>
                                </li>
                                
                            </ul>
                        </nav>
                    </div>

                    <div class="col-6 text-right">
                        <ul class="mbl d-lg-none">
                            <li class="search-button">
                                <div class="sb-search">
                                    <form>
                                        <input class="sb-search-input" onkeyup="buttonUp();" placeholder="Search"  type="search" value="" name="search">
                                        <input class="sb-search-submit" type="submit"  value="">
                                        <span class="sb-icon-search"><i class="ei ei-search"></i></span>
                                    </form>
                                </div>
                            </li>
                          
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Start Mobile Menu -->
            <div class="mobile-menu-area d-lg-none">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav class="mobile-menu-active">
                                    <ul>
                                        <li class="{{active_link('')}}">
                                            <a href="{{route('front.get.home.index')}}">Home</a>
                                        </li>
                                        <li class="{{active_link('products')}}">
                                            <a href="{{route('front.get.product.all')}}">Products</a>
                                        </li>
                                        <li class="c-dropdowns {{active_link('services')}}">
                                            <a href="{{route('front.get.service.all')}}">Services</a>
                                            <ul class="cr-dropdown-menu">
                                                @foreach($services as $ser)
                                                <li>
                                                    <a href="{{route('front.get.service.show',[$ser->slug])}}">
                                                        {{\Str::Words($ser->name,3,'')}}
                                                    </a>
                                                </li>
                                                @endforeach
                                            
                                            </ul>
                                        </li>
                                    

                                        <li class="{{active_link('blog')}}">
                                            <a href="{{route('front.get.blog.index')}}">Blog</a>
                                        </li>
                                        <li class="{{active_link('About')}}">
                                            <a href="{{route('front.get.home.about')}}">About</a>
                                        </li>
                                        <li class="{{active_link('contact-us')}}">
                                            <a href="{{route('front.get.contactus.contact')}}">Contact</a>
                                        </li>
                                
                                    </ul>
                                </nav>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Mobile Menu -->
        </div>        
    </section>
    <!-- End: Navigation
    ============================= -->