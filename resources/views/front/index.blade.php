

@extends('front.main')


@section('content')

    <!-- Start: Header Slider
    ============================= -->
    <header>
        <div class="row">
            <div class="col-md-12">
                <div class="header-slider">

                    @foreach($slider as $sl)
                    <div class="header-single-slider">
                        <figure>
                            <img src="{{ getImage(SLIDER_PATH.$sl->img)}}" alt="{{$sl->name}}">
                            <figcaption>
                                <div class="content">
                                    <div class="container inner-content text-left">
                                        <h3>Welcome To Hantus Spa</h3>
                                        <h1>{{$sl->name}}</h1>
                                        <p>{{$sl->description}}</p>
                                        @if($sl->link)
                                        <a href="{{$sl->link}}" target="_blank" title="{{$sl->name}}" class="boxed-btn">@lang('frontSite.makeAppoint')</a>
                                        @endif
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </header>

    <!-- End: Header Slider
    ============================= -->

    <!-- Start: Contact
    ============================= -->

    <section id="contact">
        <div class="container contact-wrapper text-center text-xl-left">
            <div class="row">
                <div class="col-xl-4 col-md-6 col-sm-6 single-contact">
                    <img src="{{ furl()}}/img/icons/icon01.png" alt="">
                    <h4>Opening Time</h4>
                    <p>Mon - Sat: 10h00 - 18h00</p>
                </div>
                <div class="col-xl-4 col-md-6 col-sm-6 single-contact">
                    <img src="{{ furl()}}/img/icons/icon02.png" alt="">
                    <h4>Address</h4>
                    <p> {{$setting->address}} </p>
                </div>
                <div class="col-xl-4 offset-xl-0 col-md-6 col-sm-6 offset-sm-3 single-contact">
                    <img src="{{ furl()}}/img/icons/icon03.png" alt="">
                    <h4>Telephone</h4>
                    <p>{{$setting->mobile}}</p>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Contact
    ============================= -->


    <!-- Start: Our Service
    ============================= -->
    <section id="services" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <h2>Our Services</h2>
                        <hr>
                        <p>These are the services we provide, these makes us stand apart.</p>
                    </div>
                </div>
            </div>
            <div class="row">

                @foreach($services as $ser)
                <div class="col-lg-3 col-md-6 col-sm-6 mb-5 mb-lg-0">                    
                    <div class="service-box text-center" style="    margin-bottom: 25px;">                        
                        <figure>
                            <img src="{{ getImage(SERVICE_PATH.'small/'.$ser->img)}}" alt="">
                            <figcaption>
                                <div class="inner-text">
                                    <a href="{{route('front.get.service.show',[$ser->slug])}}" class="boxed-btn">@lang('frontSite.bookNow')</a>
                                </div>
                            </figcaption>
                        </figure>
                        <h4>{{\Str::Words($ser->name,3,'')}}</h4>
                        <p>{{\Str::Words($ser->small_description,13,'')}}</p>
                        <p class="price">$ {{$ser->price}}</p>
                    </div>
                </div>
                @endforeach

              
            </div>
        </div>
    </section>

    <!-- End:  Our Service
    ============================= -->





    <!-- Start: Feature
    ============================= -->
    <section id="feature" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <h2>Feature</h2>
                        <hr>
                        <p>How to Have a Healthier and More Productive Home Office</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 mb-5 mb-lg-0">                    
                    <div class="feature-box text-center">                        
                        <div class="feature-icon">
                            <img src="{{ furl()}}/img/feature-icon/feature-icon01.png" alt="">
                        </div>
                        <h4>Hair Expert</h4>
                        <p>Lorem Ipsum is simply dummy text  the printing and typesetting.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 mb-5 mb-lg-0">                    
                    <div class="feature-box text-center">                        
                        <div class="feature-icon">
                            <img src="{{ furl()}}/img/feature-icon/feature-icon02.png" alt="">
                        </div>
                        <h4>Stone Massage</h4>
                        <p>Lorem Ipsum is simply dummy text  the printing and typesetting.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 mb-5 mb-sm-0">                    
                    <div class="feature-box text-center">                        
                        <div class="feature-icon">
                            <img src="{{ furl()}}/img/feature-icon/feature-icon03.png" alt="">
                        </div>
                        <h4>Nail Care</h4>
                        <p>Lorem Ipsum is simply dummy text  the printing and typesetting.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">                    
                    <div class="feature-box text-center">                        
                        <div class="feature-icon">
                            <img src="{{ furl()}}/img/feature-icon/feature-icon04.png" alt="">
                        </div>
                        <h4>Aromatherapy Nature</h4>
                        <p>Lorem Ipsum is simply dummy text  the printing and typesetting.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End:  Our Feature
    ============================= -->




    <!-- Start: Fun Fact
    ============================= -->

    <section id="counter" class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 single-box mb-5 mb-lg-0">
                    <img src="{{ furl()}}/img/counter/counter-icon01.png" alt="">
                    <h3><span class="counter">870</span></h3>
                    <p>Win Awards</p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 single-box mb-5 mb-lg-0">
                    <img src="{{ furl()}}/img/counter/counter-icon02.png" alt="">
                    <h3><span class="counter">1523</span></h3>
                    <p>Happy Clients</p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 single-box mb-5 mb-sm-0">
                    <img src="{{ furl()}}/img/counter/counter-icon03.png" alt="">
                    <h3><span class="counter">100</span></h3>
                    <p>Treatments</p>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 single-box">
                    <img src="{{ furl()}}/img/counter/counter-icon04.png" alt="">
                    <h3><span class="counter">50</span></h3>
                    <p>Therapists</p>
                </div>
            </div>
        </div>
    </section>

    <!-- End:  Fun Fact
    ============================= -->


    <!-- Start: Our Product
    ============================= -->
    
    <section id="product" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <h2>Our Product</h2>
                        <hr>
                        <p>We are using only the high quality original product</p>
                    </div>
                </div>                
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="product-carousel">

                        @foreach($products as $pro)
                        <div class="single-product text-center">
                            <span class="sale">Sale!</span>
                            <div class="product-img">
                                <img src="{{ getImage(PRODUCT_PATH.'small/'.$pro->img)}}" alt="{{$pro->name}}">
                            </div>
                            <ul class="rate">
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                                <li><i class="far fa-star"></i></li>
                            </ul>
                            <h5>
                                {{\Str::Words($pro->name,4,'')}}
                            </h5>
                            <div class="price">$ {{$pro->price}} </div>

                            <div class="overlay">
                                <ul class="icons">
                                    <li>
                                        <a href="{{route('front.get.product.show',[$pro->slug])}}">
                                            <i class="fas fa-link"></i>
                                        </a>
                                    </li>
                                    <li><a href="{{ getImage(PRODUCT_PATH.'small/'.$pro->img)}}" class="mfp-popup"><i class="fas fa-eye"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Our Product
    ============================= -->


    <!-- Start: Testimonial
    ============================= -->
    
    <section id="testimonial" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial-carousel text-center">

                        @foreach($testi as $tes)
                        <div class="single-testimonial">
                            <p>“{{$tes->desc}}”</p>
                            <h5>{{$tes->name}} </h5>
                            <p class="title">{{$tes->position}}</p>
                        </div>
                        @endforeach
                       
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Testimonial
    ============================= -->

    <!-- Start: Expert Beauticians
    ============================= -->
    
    <section id="beauticians" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <h2>Expert Beauticians</h2>
                        <hr>
                        <p>These are the people behind our success and failures. These guys never lose a heart.</p>
                    </div>
                </div>                
            </div>

            <div class="row">

                @foreach($team as $tea)
                <div class="col-lg-3 col-md-6 col-sm-6 mb-4 mb-lg-0">
                    <div class="single-beauticians">
                        <div class="img-wrapper">
                            <img src="{{ getImage(TEAM_PATH.$tea->img)}}" alt="{{$tea->name}}">
                            <div class="beautician-footer-text">
                                <h5> {{$tea->name}} </h5>
                                <p>{{$tea->position}}</p>
                            </div>
                        </div>
                        <div class="beautician-content">
                            <div class="inner-content">
                                <h5>{{$tea->name}}</h5>
                                <p class="title">{{$tea->position}}</p>
                                <p>{{$tea->desc}}</p>
                                <ul class="social">
                                    @if($tea->facebook)
                                    <li><a href="{{$tea->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                                    @endif
                                     @if($tea->twitter)
                                    <li><a href="{{$tea->twitter}}"><i class="fab fa-twitter"></i></a></li>
                                    @endif
                                     @if($tea->instagram)
                                    <li><a href="{{$tea->instagram}}"><i class="fab fa-instagram"></i></a></li>
                                    @endif
                                     @if($tea->linkedin)
                                    <li><a href="{{$tea->linkedin}}"><i class="fab fa-linkedin"></i></a></li>
                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
 
            </div>
        </div>
    </section>

    <!-- End: Expert Beauticians
    ============================= -->

    <!-- Start: Appoinment
    ============================= -->
    
    <section id="appoinment" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="opening-hours">
                        <h3>Opening Hours</h3>
                        <p>A collection of textile samples lay spread out on the table Samsa was a travelling salesman.</p>
                        <ul>
                            <li>Monday :    8:00am - 21:00pm</li>
                            <li>Monday :    8:00am - 21:00pm</li>
                            <li>Monday :    8:00am - 21:00pm</li>
                            <li>Monday :    8:00am - 21:00pm</li>
                            <li>Sunday:     Close</li>
                        </ul>
                    </div>
                    <div class="appoinment-wrapper">
                        <form  id="contact-form">
                            

                            @csrf
                            <div class="col-md-12">
                                <ul id="errors"></ul>
                            </div>
                            <span class="input input--hantus">
                                <input required placeholder="Name" class="input__field input__field--hantus" type="text" id="input-08" name="name" />
                              
                            </span>


                            <span class="input input--hantus">
                                <input required placeholder="Email" name="email" class="input__field input__field--hantus" type="email" id="input-011"  />
                         
                            </span>


                            <span class="input input--hantus">
                                <input placeholder="Phone" name="phone" class="input__field input__field--hantus" type="number" id="input-09"  />
                          
                            </span>
        
                            <span class="input input--hantus textarea">
                                <textarea required   placeholder="Message" name="message" class="input__field input__field--hantus" rows="5" id="input-10"></textarea>
                            
                            </span>

                            <button type="submit" class="boxed-btn">@lang('frontSite.submit')</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Appoinment
    ============================= -->


    <!-- Start: Our partner
    ============================= -->

    <section id="partner" class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="partner-carousel">

                        @foreach($brands as $par)
                        <div class="single-partner">
                            <div class="inner-partner">
                                <img src="{{ getImage(BRAND_PATH.$par->img)}}" alt="{{$par->name}}">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End: Our Partner
    ============================= -->




@endsection



@section('script')

<script type="text/javascript">
    $('#contact-form').parsley();
</script>
<script type="text/javascript">
  


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#contact-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#contact-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.contactus.sendMessage')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.input--hantus').find("input,textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection

