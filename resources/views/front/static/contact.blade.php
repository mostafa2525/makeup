@extends('front.main')


@section('content')






    <!-- Start: Breadcrumb Area
    ============================= -->

    <section id="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Contact</h2>
                    <ul class="breadcrumb-nav list-inline">
                        <li><a href="{{ route('front.get.home.index') }}">Home</a></li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Breadcrumb Area
    ============================= -->


    <!-- Start: Contact
    ============================= -->
    
    <section id="contact" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="map">
                    	{!! $setting->map !!}
                    </div>
                </div>
                <div class="col-lg-6 contact-form">
                    <h2>Contact Form</h2>
                   <form  id="contact-form">
                            

                            @csrf
                            <div class="col-md-12">
                                <ul id="errors"></ul>
                            </div>
                            <span class="input input--hantus">
                                <input required placeholder="Name" class="input__field input__field--hantus" type="text" id="input-08" name="name" />
                              
                            </span>


                            <span class="input input--hantus">
                                <input required placeholder="Email" name="email" class="input__field input__field--hantus" type="email" id="input-011"  />
                         
                            </span>


                            <span class="input input--hantus">
                                <input placeholder="Phone" name="phone" class="input__field input__field--hantus" type="number" id="input-09"  />
                          
                            </span>
        
                            <span class="input input--hantus textarea">
                                <textarea required   placeholder="Message" name="message" class="input__field input__field--hantus" rows="5" id="input-10"></textarea>
                            
                            </span>

                            <button type="submit" class="boxed-btn">@lang('frontSite.submit')</button>
                        </form>
                </div>
                <div class="col-lg-6 contact-info mt-5 mt-lg-0">
                    <h2>Contact Information</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>

                    <div class="info-box mt-4">
                        <i class="fas fa-map-marker"></i>
                        <h4> Address:</h4>
                        <p>{{$setting->address}} <br> {{$setting->mobile}} <br> {{$setting->email}}  </p>
                    </div>

                    <div class="info-box">
                        <i class="fas fa-clock"></i>
                        <h4>Opening Hours:</h4>
                        <p>Monday-Friday: 10 Am to 6 Pm <br> Saturday: 10 Am to 6 Pm <br> Sunday: Close </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- End: Contact
    ============================= -->




@endsection


@section('script')
<script type="text/javascript">
  
    $('#contact-form').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#contact-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#contact-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.contactus.sendMessage')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.input--hantus').find("input,textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection