@extends('front.main')


@section('content')


  <section id="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>About Us</h2>
                    <ul class="breadcrumb-nav list-inline">
                        <li><a href="{{route('front.get.home.index')}}">Home</a></li>
                        <li>About Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- End: Breadcrumb Area
    ============================= -->



       <!-- Start: Why choose us
    ============================= -->

    <section id="wcu">
        <div class="container">
            <div class="row">      
                <div class="video-section">
                    <div class="play-icon">
                        <a href="https://www.youtube.com/watch?v=pGbIOC83-So&amp;t=35s" class="playbtn mfp-iframe"><img src="{{ furl()}}//img/about-page/play-icon.png" alt=""></a>
                    </div>
                    <div class="watch-more">
                        <a href="#">Watch more <i class="fab fa-youtube"></i></a>
                    </div>
                </div>
				<?php $who_us = json_decode($setting->who_us); ?>
                <div class="offset-lg-6 col-lg-6 col-12 order-1 order-lg-2 wcu-content">
                    <h2>{{json_data($who_us,'aboutUsTitle')}}</h2>
                    <p><b> {{json_data($who_us,'aboutUsSmallDescription')}}</b></p>
                    {!! json_data($who_us,'aboutUsDescription') !!}
                </div>
            </div>
        </div>
    </section>
    <!-- End: Why choose us
    ============================= -->








    <!-- Start: Expert Beauticians
    ============================= -->
    
    <section id="beauticians" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12 text-center">
                    <div class="section-title">
                        <h2>Expert Beauticians</h2>
                        <hr>
                        <p>These are the people behind our success and failures. These guys never lose a heart.</p>
                    </div>
                </div>                
            </div>

            <div class="row">

                @foreach($team as $tea)
                <div class="col-lg-3 col-md-6 col-sm-6 mb-4 mb-lg-0">
                    <div class="single-beauticians">
                        <div class="img-wrapper">
                            <img src="{{ getImage(TEAM_PATH.$tea->img)}}" alt="{{$tea->name}}">
                            <div class="beautician-footer-text">
                                <h5> {{$tea->name}} </h5>
                                <p>{{$tea->position}}</p>
                            </div>
                        </div>
                        <div class="beautician-content">
                            <div class="inner-content">
                                <h5>{{$tea->name}}</h5>
                                <p class="title">{{$tea->position}}</p>
                                <p>{{$tea->desc}}</p>
                                <ul class="social">
                                    @if($tea->facebook)
                                    <li><a href="{{$tea->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                                    @endif
                                     @if($tea->twitter)
                                    <li><a href="{{$tea->twitter}}"><i class="fab fa-twitter"></i></a></li>
                                    @endif
                                     @if($tea->instagram)
                                    <li><a href="{{$tea->instagram}}"><i class="fab fa-instagram"></i></a></li>
                                    @endif
                                     @if($tea->linkedin)
                                    <li><a href="{{$tea->linkedin}}"><i class="fab fa-linkedin"></i></a></li>
                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
 
            </div>
        </div>
    </section>

    <!-- End: Expert Beauticians
    ============================= -->







@endsection